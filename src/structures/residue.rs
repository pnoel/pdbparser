use super::atom::Atom;
use super::*;

/// A [`Residue`] is a sub-structure linked to a [`Chain`].
/// It contain one or more [`Atom`]
/// It stores the following properties:
/// - res name;
/// - res number (resid);
/// - res icode (insertion code)
/// - a list of atom(s)
///
#[derive(Debug)]
pub struct Residue {
    pub name: String,
    pub res_num: u64,
    pub res_icode: Option<char>,
    pub lst_atom: Vec<Atom>,
}

impl Residue {
    /// Create a new [`Residue`] structure with an empty list of atom and no insertion code (None).
    /// The Residue have a name and a number
    ///
    /// # Examples
    ///
    /// ````
    /// use lib3dmol;
    ///
    /// let lys = lib3dmol::structures::Residue::new(String::from("lysine"), 1);
    ///
    /// ````
    pub fn new(name: String, res_num: u64) -> Residue {
        Residue {
            name,
            res_num,
            res_icode: None,
            lst_atom: Vec::new(),
        }
    }

    /// Get the name of the residue
    ///
    pub fn name(&self) -> String {
        self.name.clone()
    }

    /// Get the residue ID of the [`Residue`]
    ///
    pub fn get_res_num(&self) -> u64 {
        self.res_num
    }

    /// Get the number of [`Atom`] in the [`Residue`]
    ///
    /// # Examples
    ///
    /// ````
    /// use lib3dmol;
    ///
    /// let lys = lib3dmol::structures::Residue::new(String::from("lysine"), 1);
    /// assert_eq!(0, lys.get_number_atom());
    ///
    /// ````
    pub fn get_number_atom(&self) -> u64 {
        self.lst_atom.len() as u64
    }

    /// Add an [`Atom`] structure to the [`Residue`]
    ///
    /// # Examples
    ///
    /// ````
    /// use lib3dmol;
    /// use lib3dmol::structures::atom::AtomType;
    ///
    /// let mut lys = lib3dmol::structures::residue::Residue::new(String::from("lysine"), 1);
    /// let carbon = lib3dmol::structures::Atom::new(String::from("C"), AtomType::Carbon, 1, [0.0, 0.0, 0.0]);
    ///
    /// lys.add_atom(carbon);
    ///
    /// assert_eq!(1, lys.get_number_atom());
    ///
    /// ````
    pub fn add_atom(&mut self, a: Atom) {
        self.lst_atom.push(a);
    }

    /// Remove [`Atom`] of the [`Residue`] according to its atom_number
    /// Return Ok if the atom is correctly removed and an error if the atom cannot be
    /// find
    ///
    /// # Examples
    ///
    /// ```
    /// use lib3dmol;
    /// use lib3dmol::structures::atom::AtomType;
    ///
    /// let mut lys = lib3dmol::structures::Residue::new(String::from("lysine"), 1);
    /// let carbon = lib3dmol::structures::Atom::new(String::from("CA"), AtomType::Carbon, 1, [0.0, 0.0, 0.0]);
    /// let hydrogen = lib3dmol::structures::Atom::new(String::from("HA1"), AtomType::Hydrogen, 2, [0.0, 0.0, 0.0]);
    ///
    /// lys.add_atom(carbon);
    /// lys.add_atom(hydrogen);
    ///
    /// assert_eq!(lys.get_number_atom(), 2);
    /// lys.remove_atom(2);
    /// assert_eq!(lys.get_number_atom(), 1);
    /// ```
    pub fn remove_atom(&mut self, n: u64) {
        for index in 0..self.lst_atom.len() {
            if self.lst_atom[index].number == n {
                self.lst_atom.remove(index);
                return;
            }
        }
    }
}

impl GetAtom for Residue {
    fn get_atom(&self) -> Vec<&atom::Atom> {
        let mut lst_atom: Vec<&atom::Atom> = Vec::new();
        for atom in &self.lst_atom {
            lst_atom.push(&atom)
        }
        lst_atom
    }

    fn compute_weight(&self) -> f32 {
        self.get_atom().iter().map(|x| x.get_weight()).sum()
    }
}

/// Enumerate all Amino acid types
pub enum ResidueType {
    Ala,
    Arg,
    Asp,
    Asn,
    Cys,
    Glu,
    Gln,
    Gly,
    His,
    Ile,
    Leu,
    Lys,
    Met,
    Phe,
    Pro,
    Ser,
    Thr,
    Trp,
    Tyr,
    Val,
}
