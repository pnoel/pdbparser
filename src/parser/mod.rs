//! Functions to parse files or text and create [`Structure`]

pub mod read_pdb;

#[doc(inline)]
pub use read_pdb::read_pdb;
#[doc(inline)]
pub use read_pdb::read_pdb_txt;
