# Lib3dmol 
[![Crates.io](https://img.shields.io/crates/v/lib3dmol?logo=crates.io)](https://crates.io/crates/lib3dmol)
[![Crates.io](https://img.shields.io/crates/l/lib3dmol)](https://gitlab.inria.fr/capsid.crates/lib3dmol/blob/master/LICENSE)

**Lib3dmol is a library written in rust to read and select atoms in protein structure files in the [PDB format](http://www.wwpdb.org/documentation/file-format)**

**Documentation here**: [gitlab-pages lib3dmol](https://capsid.crates.gitlabpages.inria.fr/lib3dmol/lib3dmol/index.html)

## Usage

Add this to your `Cargo.toml`:

```toml
[dependencies]
lib3dmol = "0.3.2"
```

Here's a simple example that read a pdb file in tests/tests_file

```rust
use lib3dmol::parser;

fn main() {
    let my_structure = parser::read_pdb("tests/tests_file/f2.pdb", "Protein f2");

    println!(
        "Structure name: {}
Number of chain: {}
Number of residue: {}
Number of atom: {}",
        my_structure.name,
        my_structure.get_chain_number(),
        my_structure.get_residue_number(),
        my_structure.get_atom_number()
    );

    // Now we will extract the backbone

    let backbone = my_structure.select_atoms("backbone").unwrap();

    println!(
        "Number of chain: {}
Number of residue: {}
Number of atom: {}",
        backbone.get_chain_number(),
        backbone.get_residue_number(),
        backbone.get_atom_number()
    );
}
```

## Todo

- [X] : PDB Writer
- [X] : Structure to keep informations on nucleic acid/lipid/water
- [X] : More options to select atoms (Alpha carbon, atoms near to an other, ...)
- [ ] : Support of PDBx/mmCIF format 
