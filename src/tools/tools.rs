use std::collections::HashMap;
use std::env;
use std::fs;
use std::fs::File;
use std::io::prelude::BufRead;
use std::io::BufReader;
use std::process;

use crate::structures::atom::Atom;
use crate::structures::structure::Structure;
use crate::structures::*;

/// Convert the all amino acid to a FASTA sequence (1 residue as 1 char)
/// Consult the corresponding table to have the code 1 letter <-> 3 letters
/// [Wikipedia amino acid](https://en.wikipedia.org/wiki/Amino_acid)
///
/// # Examples
/// ```
/// use lib3dmol::{parser, tools};
///
/// let my_struct = parser::read_pdb("tests/tests_file/f2.pdb", "f2");
/// assert_eq!("TSPQPYSIERTIRWLTYQVANSLALVSEADKIMQTEYMKMIQNSGEITDRGEAILRLLKTNKHYEH", tools::fasta_seq(&my_struct));
/// ```
pub fn fasta_seq(my_struct: &Structure) -> String {
    let res: HashMap<&str, char> = [
        ("ARG", 'R'),
        ("LYS", 'K'),
        ("ASN", 'N'),
        ("ASP", 'D'),
        ("GLU", 'E'),
        ("SER", 'S'),
        ("THR", 'T'),
        ("GLN", 'Q'),
        ("CYS", 'C'),
        ("HIS", 'H'),
        ("HSD", 'H'),
        ("HSP", 'H'),
        ("HSD", 'H'),
        ("SEC", 'U'),
        ("GLY", 'G'),
        ("PRO", 'P'),
        ("ALA", 'A'),
        ("VAL", 'V'),
        ("ILE", 'I'),
        ("LEU", 'L'),
        ("MET", 'M'),
        ("PHE", 'P'),
        ("TYR", 'Y'),
        ("TRP", 'W'),
    ]
    .iter()
    .cloned()
    .collect();

    //TODO: Change the with_capacity(my_struct.get_residue_number()) because get all residue (dna, lipid, etc..)
    let mut fasta = String::with_capacity(my_struct.get_residue_number() as usize);

    for chain in &my_struct.chains {
        for residue in &chain.lst_res {
            if let Some(r) = res.get(&residue.name()[..]) {
                fasta.push(*r);
            }
        }
    }
    fasta
}

/// Compute RMSD distance between 2 structures wich have the GetAtom trait
///
/// Be careful, you should check if atoms are the same
///
/// # Example:
/// ```
/// use lib3dmol::{parser, tools};
///
/// // f2 and f2_adn are the same protein in files
/// let my_first_prot = parser::read_pdb("tests/tests_file/f2.pdb", "f2");
/// let my_second_prot = parser::read_pdb("tests/tests_file/f2_adn.pdb", "f2_adn");
///
/// let my_second_prot = my_second_prot.select_atoms("chain A").unwrap();
///
/// let rmsd = tools::rmsd(&my_first_prot, &my_second_prot);
///
/// assert_eq!(Ok(0.), rmsd)
/// ```
pub fn rmsd<T: GetAtom>(first: &T, second: &T) -> Result<f32, &'static str> {
    let atoms_first = first.get_atom();
    let atoms_second = second.get_atom();

    // RMSD can be compute only if structures have same number of atoms
    if atoms_first.len() != atoms_second.len() {
        return Err("Different number of atoms");
    }
    let mut total = 0.0;
    for index in 0..atoms_first.len() {
        let coord_first = &atoms_first[index].coord;
        let coord_second = &atoms_second[index].coord;
        total += (coord_first[0] - coord_second[0]).powi(2)
            + (coord_first[1] - coord_second[1]).powi(2)
            + (coord_first[2] - coord_second[2]).powi(2)
    }
    Ok((total / atoms_first.len() as f32).sqrt())
}

/// Extract atom mass from Charmm parameter files.
/// Charmm files must have the "prm" extension
///
pub fn atom_mass() -> HashMap<String, f32> {
    let mut ref_masses: HashMap<String, f32> = HashMap::new();

    // If CHARMM_FOLDER enviroment variable is defined, then look for .parm files in this folder. Else use the ./datasets folder*.
    let key = "CHARMM_FOLDER";
    let charmm_folder = match env::var(key) {
        Ok(charmm_folder) => charmm_folder,
        _ => "./datasets".to_string(),
    };
    let paths = match fs::read_dir(charmm_folder) {
        Ok(paths) => paths,
        Err(_e) => {
            eprintln!("cannot find your charmm folder");
            process::exit(1);
        }
    };

    for path in paths {
        let test = path.unwrap().path();
        if test.is_file() && test.extension().unwrap().to_str() == Some("prm") {
            let charmmf = match File::open(test) {
                Ok(charmmf) => charmmf,
                Err(e) => {
                    eprintln!("Error while reading {}", e);
                    process::exit(1);
                }
            };

            let reader = BufReader::new(charmmf);
            for line in reader.lines() {
                // MASS  -1  H          1.00800 ! polar H
                let l = line.unwrap();
                if l.starts_with("MASS") {
                    let split: Vec<&str> = l.split_ascii_whitespace().collect();
                    let val = match split[3].parse::<f32>() {
                        Ok(val) => val,
                        Err(e) => {
                            eprintln!("Cannot cast {} into f32", e);
                            process::exit(1);
                        }
                    };
                    ref_masses.insert(split[2].to_string(), val);
                }
            }
        }
    }
    return ref_masses;
}

/// Implement the GetAtom trait on vector of Atoms
impl GetAtom for std::vec::Vec<Atom> {
    fn get_atom(&self) -> Vec<&Atom> {
        let mut lst_atom: Vec<&Atom> = Vec::with_capacity(self.len());
        for atom in self.iter() {
            lst_atom.push(&atom)
        }
        lst_atom
    }

    fn compute_weight(&self) -> f32 {
        self.get_atom().iter().map(|x| x.get_weight()).sum()
    }
}
