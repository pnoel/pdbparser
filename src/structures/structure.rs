use super::atom::{Atom, AtomType};
use super::chain::{Chain, ChainTypes};
use super::residue::Residue;
use super::*;

use selection_atom;

/// A [`Structure`] is the SUPER structure which contain molecules like Protein, DNA, etc.
///
pub struct Structure {
    pub name: String,
    pub chains: Vec<Chain>,
    last_chain_added: char,
}

impl Structure {
    /// Create a new [`Structure`]
    ///
    /// # Examples
    ///
    /// ````
    /// use lib3dmol::structures::structure;
    ///
    /// let my_prot = structure::Structure::new(String::from("my_struct"));
    ///
    /// ````
    pub fn new(n: String) -> Structure {
        Structure {
            name: n,
            chains: Vec::new(),
            last_chain_added: ' ',
        }
    }

    /// Get the name of the [`Structure`]
    ///
    /// # Examples
    ///
    /// ````
    /// use lib3dmol::structures::structure;
    ///
    /// let my_struct = structure::Structure::new(String::from("my_struct"));
    ///
    /// assert_eq!("my_struct", my_struct.name());
    ///
    /// ````
    pub fn name(&self) -> &str {
        &self.name
    }

    /// Add a new [`Chain`] in the [`Structure`]
    ///
    /// # Examples
    ///
    /// ````
    /// use lib3dmol::structures::{structure, chain};
    ///
    /// let mut my_struct = structure::Structure::new(String::from("my_struct"));
    /// let my_chain_a = chain::Chain::new('a', chain::ChainTypes::Lipid);
    ///
    /// my_struct.add_chain(my_chain_a);
    ///
    /// assert_eq!(1, my_struct.get_chain_number());
    ///
    /// ````
    pub fn add_chain(&mut self, c: Chain) {
        self.last_chain_added = c.get_name();
        self.chains.push(c);
    }

    /// Return True if the [`Chain`] is in the [`Structure`]
    ///
    /// # Examples
    ///
    /// ````
    /// use lib3dmol::structures::{structure, chain};
    ///
    /// let mut my_struct = structure::Structure::new(String::from("my_struct"));
    /// let my_chain_a = chain::Chain::new('a', chain::ChainTypes::Protein);
    /// my_struct.add_chain(my_chain_a);
    ///
    /// assert!(my_struct.is_chain('a'));
    ///
    /// ````
    pub fn is_chain(&self, c: char) -> bool {
        for ii in &self.chains {
            if ii.get_name() == c {
                return true;
            }
        }
        false
    }

    /// Get the number of [`Chain`] in the [`Structure`]
    ///
    /// # Examples
    ///
    /// ````
    /// use lib3dmol::structures::structure;
    ///
    /// let my_struct = structure::Structure::new(String::from("my_struct"));
    ///
    /// assert_eq!(0, my_struct.get_chain_number());
    /// ````
    pub fn get_chain_number(&self) -> usize {
        self.chains.len()
    }

    /// Return the number of [`Residue`] in the [`Structure`]
    ///
    /// According to PDB format, residue can be molecules of amino acid, nucleic acid, lipid or ligand
    ///
    /// # Examples
    ///
    /// ````
    /// use lib3dmol::structures::{structure, chain, residue};
    ///
    /// let mut my_struct = structure::Structure::new(String::from("my_struct"));
    /// let mut my_chain = chain::Chain::new('n', chain::ChainTypes::Protein);
    /// let lys = residue::Residue::new(String::from("lysine"), 1);
    /// let pro = residue::Residue::new(String::from("proline"), 2);
    ///
    /// my_chain.add_res(lys);
    /// my_chain.add_res(pro);
    /// my_struct.add_chain(my_chain);
    ///
    /// assert_eq!(2, my_struct.get_residue_number());
    ///
    /// ````
    pub fn get_residue_number(&self) -> u64 {
        let mut n: u64 = 0;
        for chain in self.chains.iter() {
            for _ in chain.lst_res.iter() {
                n += 1;
            }
        }
        n
    }

    /// Return the number of [`Atom`] in the [`Structure`]
    ///
    /// # Examples
    ///
    /// ```
    /// use lib3dmol::parser;
    ///
    /// let my_struct = parser::read_pdb("tests/tests_file/f2.pdb", "f2");
    /// assert_eq!(1085, my_struct.get_atom_number());
    /// ```
    pub fn get_atom_number(&self) -> u64 {
        let mut n: u64 = 0;
        for chain in self.chains.iter() {
            for res in chain.lst_res.iter() {
                for _ in res.lst_atom.iter() {
                    n += 1;
                }
            }
        }
        n
    }

    /// Generate a [`Vector`] of atoms index (u64)
    /// Can be used in other program like rrmsd_map to select specific atoms
    ///
    /// # Examples
    /// ```
    /// use lib3dmol::parser;
    ///
    /// let my_struct = parser::read_pdb("tests/tests_file/f2_adn.pdb", "f2");
    /// let atom_index = my_struct.get_atom_index();
    ///
    /// assert_eq!(atom_index[0], 1);
    /// assert_eq!(atom_index[1], 2);
    /// ```
    pub fn get_atom_index(&self) -> Vec<u64> {
        let mut lst: Vec<u64> = Vec::new();
        for chain in &self.chains {
            for res in &chain.lst_res {
                for atom in &res.lst_atom {
                    lst.push(atom.number);
                }
            }
        }
        lst
    }

    /// Return a mutable reference of a [`Chain`] with its name. Return None if the
    /// [`Chain`] does not exist
    ///
    /// # Examples
    ///
    /// ````
    /// use lib3dmol::structures::{structure, chain};
    ///
    /// let mut my_struct = structure::Structure::new(String::from("my_struct"));
    /// my_struct.add_chain(chain::Chain::new('n', chain::ChainTypes::Protein));
    /// assert_eq!('n', my_struct.chains[0].get_name());
    /// {
    ///     let mut reference = my_struct.get_chain_ref('n').unwrap();
    ///     reference.name = 'a';
    /// }
    /// assert_eq!('a', my_struct.chains[0].get_name());
    /// ````
    pub fn get_chain_ref(&mut self, c: char) -> Option<&mut Chain> {
        for chain in &mut self.chains {
            if chain.name == c {
                return Some(chain);
            }
        }
        None
    }

    /// Function that add information on the [`Structure`] (used in the parsing)
    /// /!\Change this to a macro!
    ///
    #[warn(clippy::too_many_arguments)]
    pub fn update_structure(
        &mut self,
        chain: char,
        res_name: String,
        res_number: u64,
        res_icode: Option<char>,
        atom_name: String,
        atom_number: u64,
        a_type: AtomType,
        coord: [f32; 3],
        occupancy: Option<f32>,
        tmp_factor: Option<f32>,
        element: Option<String>,
        charge: Option<String>,
    ) {
        // Get a chain reference. If the chain exist, return a mutable reference to it. If not,
        // create a new chain an return the mutable reference
        let chain = match self.get_chain_ref(chain) {
            Some(c) => c,
            None => {
                let chain_type = ChainTypes::get(&res_name.to_uppercase()[..]);
                self.add_chain(Chain::new(chain, chain_type));
                self.get_chain_ref(chain).unwrap()
            }
        };

        // Get a residue reference. If the residue exist, return a mutable reference to it. If not,
        // create a new residue and return it as mutable reference
        let residue = match chain.get_residue_ref(res_number as u64, res_icode) {
            Some(r) => r,
            None => {
                chain.add_res(Residue{name: res_name, res_num: res_number, res_icode: res_icode, lst_atom: Vec::new() });
                chain.get_residue_ref(res_number, res_icode).unwrap()
            }
        };

        let atom = Atom::new_complete(
            atom_name,
            atom_number,
            coord,
            a_type,
            occupancy,
            tmp_factor,
            element,
            charge,
        );
        residue.add_atom(atom);
    }

    /// Select [`Atom`] from a pattern and return a new [`Structure`]
    ///
    /// The pattern could use keywords "Chain", "Resid" or "Backbone" (keyword are not case sensitive)
    ///
    /// ## "Chain"
    /// The Chain keyword is used to select chain. It must be follow by one or two chain names separate by the "to" keyword.
    /// The chain name is case sensitive.
    /// examples:
    /// "Chain A" will select only the Chain A.
    /// "Chain A to D" will select chains A, B, C and D.
    ///
    /// ## "Resid"
    /// The Resid keyword is used to select residues. It must be follow by one or two chain names separate by the "to" keyword.
    /// In case where the protein has multiple chains, the Resid will return residue(s) for all chains.
    /// examples:
    /// "Resid 1" will select only the residue 1 of each chain
    /// "Resid 12 to 50" will select residues 12, 13, .., 50 for all chains
    ///
    /// ## "Backbone"
    /// The Backbone keyword is used to select atoms in the backbone for each residues. It don't take parameters.
    ///
    /// ## Special keyword "and"
    /// You can use the keyword "and" to separate 2 or more differents selection.
    /// examples:
    /// "Chain A and Resid 40 to 150"
    ///
    /// # Examples
    ///
    /// ```
    /// use lib3dmol::parser;
    ///
    /// let my_struct = parser::read_pdb("tests/tests_file/f2.pdb", "f2");
    ///
    /// assert_eq!(66, my_struct.get_residue_number());
    /// assert_eq!(1085, my_struct.get_atom_number());
    ///
    /// let prot_backbone = my_struct.select_atoms("resid 10 to 50 and backbone").unwrap();
    ///
    /// assert_eq!(41, prot_backbone.get_residue_number());
    /// assert_eq!(164, prot_backbone.get_atom_number());
    /// ```
    // TODO: The methode is idiot and need to be improve.
    // ex: don't parse the chain if it's not selected
    pub fn select_atoms(&self, pattern: &str) -> Option<Structure> {
        let mut new_struct = Structure::new(self.name.clone());

        let select = match selection_atom::parse_select(&pattern) {
            Some(x) => x,
            None => {
                println!("Can't parse the protein with these attributes");
                return None;
            }
        };
        for chain in &self.chains {
            let c_chain = chain.name;
            for residue in &chain.lst_res {
                let c_res = residue.res_num;
                for atom in &residue.lst_atom {
                    if selection_atom::atom_match(&select, c_chain, c_res, atom.is_backbone) {
                        new_struct.update_structure(
                            c_chain,
                            residue.name.clone(),
                            c_res,
                            residue.res_icode,
                            atom.name.clone(),
                            atom.number,
                            atom.a_type.clone(),
                            atom.coord,
                            atom.occupancy,
                            atom.temp_factor,
                            atom.element.clone(),
                            atom.charge.clone(),
                        );
                    }
                }
            }
        }
        Some(new_struct)
    }

    /// Used to clean the dialing of [`Atom`] in the [`Structure`]
    /// Does not change the dialing of residues.
    ///
    ///
    /// # Examples
    /// ```
    /// use lib3dmol::parser;
    ///
    /// let my_prot = parser::read_pdb("tests/tests_file/f2.pdb", "f2");
    /// let mut backbone = my_prot.select_atoms("backbone").unwrap();
    /// let lst_atom_id = backbone.get_atom_index();
    /// assert_eq!(1, lst_atom_id[0]);
    /// assert_eq!(5, lst_atom_id[1]);
    ///
    /// backbone.refine_atom_numbering();
    /// let lst_atom_id = backbone.get_atom_index();
    /// assert_eq!(1, lst_atom_id[0]);
    /// assert_eq!(2, lst_atom_id[1]);
    /// ```
    pub fn refine_atom_numbering(&mut self) {
        let mut n_atom = 1;
        for chain in &mut self.chains {
            for residue in &mut chain.lst_res {
                for atom in &mut residue.lst_atom {
                    atom.number = n_atom;
                    n_atom += 1;
                }
            }
        }
    }

    /// Used to remove Hydrogens in the proteins
    ///
    ///
    /// # Examples
    /// ```
    /// use lib3dmol::parser;
    ///
    /// let mut my_struct = parser::read_pdb("tests/tests_file/f2.pdb", "f2");
    /// assert_eq!(1085, my_struct.get_atom_number());
    /// my_struct.remove_h();
    /// assert_eq!(541, my_struct.get_atom_number());
    /// ```
    pub fn remove_h(&mut self) {
        for chain in &mut self.chains {
            for residue in &mut chain.lst_res {
                for index in (0..residue.lst_atom.len()).rev() {
                    match residue.lst_atom[index].a_type {
                        AtomType::Hydrogen => residue.remove_atom(residue.lst_atom[index].number),
                        _ => (),
                    }
                }
            }
        }
        self.refine_atom_numbering();
    }
}

impl GetAtom for Structure {
    fn get_atom(&self) -> Vec<&Atom> {
        let mut lst_atom: Vec<&Atom> = Vec::new();
        for chain in &self.chains {
            for res in &chain.lst_res {
                for atom in &res.lst_atom {
                    lst_atom.push(&atom)
                }
            }
        }
        lst_atom
    }
    fn compute_weight(&self) -> f32 {
        self.get_atom().iter().map(|x| x.get_weight()).sum()
    }
}
