use crate::structures::{Atom, Residue, Structure};

use std::fs::File;
use std::io;
use std::io::Write;

/// Write a PDB file for the [`Structure`].
/// Be careful, the protein is write with the atom numbers in its structure. Remind to use the method
/// my_struct.refine_atom_numbering() before !
pub fn write_pdb(my_prot: &Structure, file: &str) -> io::Result<()> {
    let mut output_pdb = File::create(file)?;

    for chain in &my_prot.chains {
        for residue in &chain.lst_res {
            for atom in &residue.lst_atom {
                write!(output_pdb, "{}", format_pdb(&atom, &residue, &chain.name))?;
            }
        }
    }
    Ok(())
}

fn format_atom(atom: &Atom) -> String {
    dbg!(match atom.name.len() {
        0 => match atom.get_symbol().len() {
            1 => format!("  {} ", atom.get_symbol()),
            2 => format!(" {} ", atom.get_symbol()),
            _ => format!("    "), // Unreachable because symbol length is 1 or 2
        },
        1 => format!(" {}  ", atom.name),
        x if x == 2 && atom.get_symbol().len() == 2 => format!(" {} ", atom.get_symbol()),
        x if x == 2 && atom.get_symbol().len() == 1 => format!(" {} ", atom.name),
        x if x == 3 && atom.get_symbol().len() == 1 => format!(" {}", atom.name),
        4 => atom.name.clone(),
        _ => format!("    "), // Unreachable because atom.name cannot be > 4 and < 0
    })
}

fn format_pdb(atom: &Atom, res: &Residue, chain_name: &char) -> String {
    format!(
        "ATOM  {:>5} {:<4} {:>3} {}{:>4}    {:>8.3}{:>8.3}{:>8.3}{}{}          {:>}{}\n",
        atom.number,
        dbg!(format_atom(&atom)),
        res.name,
        chain_name,
        res.res_num,
        atom.coord[0],
        atom.coord[1],
        atom.coord[2],
        match &atom.occupancy {
            Some(o) => format!("{:>6.2}", o),
            None => String::from("      "),
        },
        match &atom.temp_factor {
            Some(t) => format!("{:>6.2}", t),
            None => String::from("      "),
        },
        match &atom.element {
            Some(e) => format!("{:>2}", e),
            None => String::from("  "),
        },
        match &atom.charge {
            Some(c) => c,
            None => "  ",
        },
    )
}

#[test]
fn format_pdb_write() {
    use crate::structures;
    use crate::structures::atom::{Atom, AtomType};

    let my_atom = Atom::new_complete(
        String::from("CA"),
        1,
        [0.1, 0.2, 0.3],
        AtomType::Carbon,
        Some(1.01),
        Some(31.02),
        Some(String::from("C")),
        Some(String::from("1+")),
    );
    let my_res = structures::Residue::new(String::from("ALA"), 1);
    let my_pdb_string = format_pdb(&my_atom, &my_res, &' ');
    let output =
        "ATOM      1  CA  ALA     1       0.100   0.200   0.300  1.01 31.02           C1+\n";
    assert_eq!(output, my_pdb_string);

    let my_atom = Atom::new_complete(
        String::from("HG21"),
        99996,
        [0.1, 0.2, 0.3],
        AtomType::Carbon,
        Some(1.01),
        None,
        Some(String::from("H")),
        None,
    );
    let my_res = structures::Residue::new(String::from("ALA"), 1);
    let my_pdb_string = format_pdb(&my_atom, &my_res, &' ');
    let output =
        "ATOM  99996 HG21 ALA     1       0.100   0.200   0.300  1.01                 H  \n";
    assert_eq!(output, my_pdb_string);
}
